package com.andkorsh.proservicetest.AccountService.testclient;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.andkorsh.proservicetest.AccountService.AccountServiceConstants;
import com.andkorsh.proservicetest.AccountService.AccountServiceInterface;

public class TestClient
{
    static class User
    {
        private Integer id;
        private boolean read, write;

        public User(Integer id)
        {
            this.id = id;
        }
    }

    private static List<User> users;
    private static int threadPoolSize;

    private static void readConfig() throws FileNotFoundException, IOException,
            ParseException
    {
        users = new ArrayList<>();

        JSONParser parser = new JSONParser();
        FileReader configFR = new FileReader(Constants.CONFIG_FILE_PATH);
        JSONObject config = (JSONObject) parser.parse(configFR);

        threadPoolSize = ((Long) config.get("threadPoolSize")).intValue();
        
        JSONArray idList = (JSONArray) config.get("idList");

        if (idList.isEmpty())
        {
            int idRangeStart = ((Long) config.get("idRangeStart")).intValue();
            int idRangeEnd = ((Long) config.get("idRangeEnd")).intValue();

            for (int i = idRangeStart; i <= idRangeEnd; i++)
                users.add(new User(i));
        }
        else
        {
            for (Object id : idList)
                users.add(new User(((Long) id).intValue()));
        }

        Collections.shuffle(users);

        long idReadCount = (Long) config.get("idReadCount");
        for (User user : users)
        {
            if (--idReadCount < 0) break;
            user.read = true;
        }

        Collections.shuffle(users);

        long idWriteCount = (Long) config.get("idWriteCount");
        for (User user : users)
        {
            if (--idWriteCount < 0) break;
            user.write = true;
        }

        Collections.shuffle(users);
    }

    public static void main(String... args) throws NotBoundException,
            FileNotFoundException, IOException, ParseException
    {
        readConfig();

        Registry registry = LocateRegistry.getRegistry(Constants.RMI_HOST,
                AccountServiceConstants.RMI_PORT);
        final AccountServiceInterface accService = (AccountServiceInterface) 
                registry.lookup(AccountServiceConstants.RMI_ID);

        final Random rand = new Random(System.currentTimeMillis());

        ExecutorService threadpool = Executors.newFixedThreadPool(
                threadPoolSize);

        final AtomicLong n = new AtomicLong(users.size());
        
        for (final User user : users)
        {
            threadpool.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            System.out.println(n.decrementAndGet() + " " + user.id);
                            
                            if (user.write)
                                accService.addAmount(user.id, rand.nextLong());
                            if (user.read)
                                accService.getAmount(user.id);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                });
        }    
    }
}
