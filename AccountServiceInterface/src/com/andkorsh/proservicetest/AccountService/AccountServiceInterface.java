package com.andkorsh.proservicetest.AccountService;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

public interface AccountServiceInterface extends Remote
{
    /**
     * Retrieves current balance or zero if addAmount() method was not called
     * before for specified id
     * 
     * @param id
     *            balance identifier
     * @throws RemoteException
     * @throws SQLException 
     */

    Long getAmount(Integer id) throws RemoteException, SQLException;

    /**
     * Increases balance or set if addAmount() method was called first time
     * 
     * @param id
     *            balance identifier
     * @param value
     *            positive or negative value, which must be added to current
     *            balance
     * @throws RemoteException
     * @throws SQLException 
     */

    void addAmount(Integer id, Long value) throws RemoteException, SQLException;
}
