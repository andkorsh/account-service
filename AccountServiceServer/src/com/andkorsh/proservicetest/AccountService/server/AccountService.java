package com.andkorsh.proservicetest.AccountService.server;

import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.andkorsh.proservicetest.AccountService.AccountServiceInterface;

public class AccountService extends UnicastRemoteObject implements
        AccountServiceInterface
{
    private static final long serialVersionUID = 1L;

    private ConcurrentMap<Integer, AtomicLong> cache;
    private Database database;
    private AtomicInteger activeThreadsCount;
    private boolean stopped = true;

    public int getActiveThreadsCount()
    {
        return activeThreadsCount.intValue();
    }
    
    public AccountService() throws RemoteException, SQLException, 
            PropertyVetoException
    {
        super();
        cache = new ConcurrentHashMap<>(Constants.CACHE_INITIAL_CAPACITY);
        database = new Database(cache);
        activeThreadsCount = new AtomicInteger(0);
        stopped = false;
    }

    private AtomicLong getAmountObject(Integer id) throws SQLException
    {
        AtomicLong amount = cache.get(id);

        if (null == amount)
            database.loadToCache(id);
        // Key already added to cache either by the database
        // or by the another thread. Null if nothing.
        amount = cache.get(id);
        return amount;
    }

    @Override
    public Long getAmount(Integer id) throws RemoteException, SQLException
    {
        if (stopped) throw new RemoteException("The server has been stopped");
        
        activeThreadsCount.incrementAndGet();
        AtomicLong amount = getAmountObject(id);
        activeThreadsCount.decrementAndGet();
        return (null == amount) ? 0L : amount.longValue();
    }

    @Override
    public void addAmount(Integer id, Long value) throws RemoteException,
            SQLException
    {
        if (stopped) throw new RemoteException("The server has been stopped");
        
        activeThreadsCount.incrementAndGet();
        AtomicLong amount = getAmountObject(id);
        // amount is null if addAmount() method was called first time
        if (null == amount)
            amount = cache.putIfAbsent(id, new AtomicLong(value));
        // putIfAbsent could return not null if id was associated
        // with another value by another thread after we got null
        if (null != amount)
            amount.addAndGet(value);
        database.saveFromCache(id);
        activeThreadsCount.decrementAndGet();
    }

    public void stop() throws InterruptedException, SQLException
    {
        stopped = true;
        while (getActiveThreadsCount() > 0)
            Thread.sleep(100);
        database.closeConnection();
    }
}
