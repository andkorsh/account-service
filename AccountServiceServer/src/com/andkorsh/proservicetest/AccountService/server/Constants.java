package com.andkorsh.proservicetest.AccountService.server;

public class Constants
{
    public static final String DB_HOST = "localhost";
    public static final String DB_NAME = "mysql";
    public static final String DB_USER = "accservice";
    public static final String DB_PWD = "password";
    public static final String DB_TABLE = "accounts";
    
    public static final int DB_MAX_POOL_SIZE = 99;
    
    public static final int CACHE_INITIAL_CAPACITY = 100;
}
