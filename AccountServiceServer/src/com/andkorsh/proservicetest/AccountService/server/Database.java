package com.andkorsh.proservicetest.AccountService.server;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class Database
{
    private ConcurrentMap<Integer, AtomicLong> cache;

    private static final String CREATE_TABLE_QUERY = 
            "create table if not exists %s "
            + "(id int unique, amount bigint)";

    private static final String GET_AMOUNT_QUERY = 
            "select amount from %s where id = %d";

    private static final String PUT_AMOUNT_QUERY = 
            "insert into %s values (%d, %d) "
            + "on duplicate key update amount = %d";

    private ComboPooledDataSource pool;
    
    public Database(ConcurrentMap<Integer, AtomicLong> cache)
            throws SQLException, PropertyVetoException
    {
        this.cache = cache;
        
        String dbUrl = String.format("jdbc:mysql://%s/%s", Constants.DB_HOST, 
                Constants.DB_NAME);
        
        pool = new ComboPooledDataSource();
        pool.setDriverClass("com.mysql.jdbc.Driver");
        pool.setJdbcUrl(dbUrl);
        pool.setUser(Constants.DB_USER);
        pool.setPassword(Constants.DB_PWD);
        pool.setMaxPoolSize(Constants.DB_MAX_POOL_SIZE);
        
        try (Connection connection = pool.getConnection())
        {
            Statement statement = connection.createStatement();
            String query = String.format(CREATE_TABLE_QUERY, 
                    Constants.DB_TABLE);
            statement.executeUpdate(query);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public void loadToCache(Integer id) throws SQLException
    {
        try (Connection connection = pool.getConnection())
        {
            Statement statement = connection.createStatement();
            String query = String.format(GET_AMOUNT_QUERY, 
                    Constants.DB_TABLE, id);
            ResultSet res = statement.executeQuery(query);
            if (res.next())
                cache.putIfAbsent(id, new AtomicLong(res.getLong(1)));
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public void saveFromCache(Integer id) throws SQLException
    {
        try (Connection connection = pool.getConnection())
        {
            long amount = cache.get(id).longValue();
            Statement statement = connection.createStatement();
            String query = String.format(PUT_AMOUNT_QUERY, Constants.DB_TABLE, 
                    id, amount, amount);
            statement.executeUpdate(query);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    
    public void closeConnection() throws SQLException
    {
        pool.close();
    }
}
