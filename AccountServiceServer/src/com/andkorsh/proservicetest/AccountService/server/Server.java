package com.andkorsh.proservicetest.AccountService.server;

import java.beans.PropertyVetoException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLException;
import java.util.Scanner;

import com.andkorsh.proservicetest.AccountService.AccountServiceConstants;

public class Server
{
    public static void main(String... args) throws RemoteException,
            AlreadyBoundException, NotBoundException, SQLException, 
            InterruptedException, PropertyVetoException
    {
        System.setProperty
            ("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
        System.setProperty
            ("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "OFF");
        
        AccountService accService = new AccountService();
        Registry registry = LocateRegistry.createRegistry(
                AccountServiceConstants.RMI_PORT);
        registry.bind(AccountServiceConstants.RMI_ID, accService);
        System.out.println(AccountServiceConstants.RMI_ID + " was started");
        
        Scanner command = new Scanner(System.in);
        
        boolean run = true;
        
        while (run)
        {
            System.out.print("server > ");
            
            switch (command.nextLine().toLowerCase())
            {
                case "stop":
                    System.out.println("Wait for unfinished " + 
                            accService.getActiveThreadsCount() + " threads");
                    accService.stop();
                    System.out.println("The server has been stopped");
                    run = false;
                    break;

                case "stats":
                    System.out.println("Server statistics:");
                    break;
                    
                case "help":
                case "?":
                    System.out.println("Possible commands include:\n"
                        + "  stop\t- Stops the server\n"
                        + "  stats\t- Shows statistics of server requests\n"
                        + "  help\t- Shows this help message");
                    break;
                default:
                    System.out.println("Unknown command. Type 'help' for a "
                            + "list of a possible commands.");
                    break;
            }
        }
        
        command.close();
        System.exit(0);
    }
}
